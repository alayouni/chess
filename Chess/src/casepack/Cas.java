package casepack;

public class Cas {
	public short l;
	public short c;
	
	public Cas(int ll,int cc){
		l=(short)ll;
		c=(short)cc;
	}
	
	public Cas(Cas pos){
		l=pos.l;
		c=pos.c;
	}
	
	public boolean egal(int ll,int cc){
		if(l==ll&&c==cc)
			return true;
		else
			return false;
	}
	
	public boolean egal(Cas pos){
		if(pos.l==l&&pos.c==c)
			return true;
		return false;
	}
}
