package components.dragndrop;
import java.awt.AlphaComposite;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class MyGlassPane extends JPanel
{
	private AlphaComposite composite;
    private DragEffect dragEffect=null;
    Point pt=null;

    public MyGlassPane(){
    	setLayout(null);
        setOpaque(false);
        composite = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.4f);
    }
    
    protected void paintComponent(Graphics g){
    	super.paintComponent(g);
    	if(dragEffect==null)
			return;
    	if(pt!=null){
			Graphics2D g2 = (Graphics2D) g;
	        g2.setComposite(composite);
	        g.translate(pt.x, pt.y);
			dragEffect.paintEffect(g);
			g.translate(-pt.x, -pt.y);
    	}
    }

    public void setDragEffect(DragEffect dragEffect){
        this.dragEffect = dragEffect;
        repaint();
    }
    
    public void setPoint(Point effectLoc){
    	if(dragEffect!=null){
    		pt=effectLoc;
    		repaint();
    	}
    }
}