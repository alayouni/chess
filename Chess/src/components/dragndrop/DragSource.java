package components.dragndrop;

import java.awt.Component;


/**
 * Each component that is intended to handle drag Events must implement this interface.
 * @see {@link DropTarget}
 */
public interface DragSource{
	/**
	 * @return an instance of {@link DragDescriptor} if a drag source was previously installed on this component 
	 * 			by the mean of {@link #setDragDescriptor(DragDescriptor)}.<br/>
	 * 			If no DragSource were installed returns null.
	 * @see #setDragDescriptor(DragDescriptor)
	 */
	public DragDescriptor getDragDescriptor();
	
	
	/**
	 * Installs a {@link DragDescriptor} on this component.<br/><br/>
	 * <b>Note: </b> To make the drag effect work on this component, <u>after</u> invoking this method,
	 *  you need to invoke the method {@link DragnDropManager#installDragEffect(Component)}.<br/><br/>
	 *
	 * this component <u>must</u> be <u>already</u> added to the hierarchy of a window which returns 
	 * an instance of MyGlassPane when invoking getGlassPane().
	 * 
	 * @param dragDescriptor the drag descriptor to be installed.
	 * @see {@link #getDragDescriptor()}<br/> 
	 * {@link DragnDropManager#installDragEffect(Component)}<br/>
	 */
	public void setDragDescriptor(DragDescriptor dragDescriptor);
}
