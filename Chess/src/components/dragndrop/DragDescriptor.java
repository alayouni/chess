package components.dragndrop;

import java.awt.Component;


/**
 * Each Component that is intended to support Drag events must implement {@link DragSource}, 
 * which requires installing an implementation of #{@link DragDescriptor}.<br/>
 * @see {@link DropTarget} and {@link DropDescriptor}
 */
public interface DragDescriptor {
	/**
	 * @return an implementation of {@link DragEffect} which draws the transparent drag effect on the glass pane. 
	 * @see #setDragEffect(DragEvent)
	 */
	public DragEffect getDragEffect();
	
	/**
	 * @return The dragged content if it exists, otherwise returns null.
	 */
	public Object getDraggedContent();
	
	/**
	 * <p>When the dragSource holding this dragDescriptor detects a mousePressed Event, it automatically invokes this method.<br/>
	 * An implementation of this method must treat the drag event e, and accordingly either describes the dragEffect to be 
	 * drawn through an implementation of {@link DragEffect}, or sets the dragEffect to null if {@link #getDraggedContent()}==null.
	 * Once this method invoked, {@link #getDragEffect()} must return the drag effect that has been set.</p>
	 * 
	 * Note that {@link #setDraggedContent(DragEvent)} will be called before calling this method.
	 * @param e
	 */
	public void setDragEffect(DragEvent e);
	
	/**
	 * <p>When the dragSource holding this dragDescriptor detects a mousePressed Event, it automatically invokes this method.<br/>
	 * An implementation of this method must treat the drag event e, and accordingly either sets the dragged content 
	 * or sets the drag content to null.
	 * Once this method invoked, {@link #getDraggedContent()} must return dragged content that has been set.</p>
	 * 
	 * <b>Note: </b>if e==null the draggedContent must be set to null.
	 * @param e 
	 */
	public void setDraggedContent(DragEvent e);
	
	/**
	 * Must return the dragSource holding this dragDescriptor.
	 * dragging events.
	 */
	public Component getComponentSourceOfDrag();
	
	
	/**
	 * If needed implements the treatments that must be performed after a successful drop operation.
	 * @param e 
	 */
	public void dropPerformed(DropEvent e);
	
	/**
	 * If needed implements the treatments that must be performed after an unsupported drop operation occurs.
	 */
	public void dropUnsupported();
}
