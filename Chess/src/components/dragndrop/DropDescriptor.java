package components.dragndrop;



/**
 * Each Component that is intended to support Drop Events must implement {@link DropTarget}, 
 * which requires installing an implementation of {@link DragDescriptor}.<br/>
 * @see {@link DragSource} and {@link DragDescriptor} 
 */
public interface DropDescriptor {
	/**
	 * <b>Note: </b> do not convert e!
	 * @return true if the drop event e is supported, otherwise returns false. 
	 */
	public boolean isDropEventSupported(DropEvent e);
	
	/**
	 * If a specific graphical effect should appear on the drop target component (other than changing the cursor 
	 * which is automatically handled) <u>when the mouse passes over it</u>, the implementation of this method must 
	 * perform the needed changes and at the end invoke  (drop target component).repaint().<br/>
	 * For example on a Tree Component if <b>e</b> allows a drop under a node, a small line should appear under that node.
	 * @param e the DropEvent which may create an effect on the component view. If <b>e</b> is null the graphical 
	 * effect must disappear.
	 */
	public void updateDropEffect(DropEvent e);
	
	/**
	 * An implementation of this method must describe the treatments to perform when a successful drop occur.<br/>
	 * When implementing this method do not check if the drop is supported because its already guaranteed i.e., this method 
	 * won't be invoked if the drop is not supported.
	 * @param e
	 */
	public void fireContentDropped(DropEvent e);
	
}
