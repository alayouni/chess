package components.dragndrop;

import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JViewport;
import javax.swing.SwingUtilities;

import cursors.Cursors;


/**
 * This class offers the static method {@link #installDragEffect(Component)} which must always be called 
 * <u>after</u> {@link DragSource#setDragDescriptor(DragDescriptor)}.
 */
public class DragnDropManager {
	private static Map<Component,MouseAdapter> dndMap=null;
	/**
	 * @see {@link#scrollIfNeeded(Component, Point, Rectangle)}
	 */
	private static final int MARGIN_OF_SCROLLING=10;
	
	/**
	 * The method {@link DragSource#setDragDescriptor(components.dragndrop.DragSource)} must be invoked from dragSource 
	 * <u>before</u> invoking this method.<br/><br/>
	 * 
	 * @param dragSource must be a component that implements {@link DragSource}. Also dragSource <u>must</u> be <u>already</u> added 
	 * to the hierarchy of a window which returns  an instance of MyGlassPane when invoking getGlassPane().
	 */
	public static void installDragEffect(Component dragSource){
		if(getDndMap().get(dragSource)!=null){
			dragSource.removeMouseListener(dndMap.get(dragSource));
			dragSource.removeMouseMotionListener(dndMap.get(dragSource));
		}
		final DragSource dragS=(DragSource)dragSource;
		dndMap.put(dragSource, new MouseAdapter() {
			Container contentPane=null;
			MyGlassPane glassPane=null;
			DragEvent draggedEvent=null;
        	DropDescriptor lastDropDescriptor=null;
			
			
			public void mousePressed(MouseEvent e) {
				if(((Component)dragS).isEnabled()){
					if(e.getButton()==MouseEvent.BUTTON1){
						if(dragS.getDragDescriptor()!=null){
							draggedEvent=new DragEvent(e.getComponent(),e.getPoint());
							dragS.getDragDescriptor().setDraggedContent(draggedEvent);
							dragS.getDragDescriptor().setDragEffect(draggedEvent);
							if(dragS.getDragDescriptor().getDraggedContent()!=null){
							      getGlassPane().setDragEffect(dragS.getDragDescriptor().getDragEffect());
							      getGlassPane().setVisible(true);
							      Point pt=(Point) e.getPoint().clone();
							      SwingUtilities.convertPointToScreen(pt, e.getComponent());
							      SwingUtilities.convertPointFromScreen(pt, getGlassPane());
							      getGlassPane().setPoint(pt);
							}
							else{
								draggedEvent=null;
								return;
							}
						}
					}
				}
			}
			
			public void mouseReleased(MouseEvent e) {
				if(((Component)dragS).isEnabled()){
					if(e.getButton()==MouseEvent.BUTTON1){
						if(dragS.getDragDescriptor()!=null){
							if(dragS.getDragDescriptor().getDraggedContent()!=null){
						        Point ptOnScreen = (Point) e.getPoint().clone();
						        SwingUtilities.convertPointToScreen(ptOnScreen, e.getComponent());
						        
						        Point p=(Point)ptOnScreen.clone();
						        SwingUtilities.convertPointFromScreen(p, getGlassPane());
						        
						        getGlassPane().setPoint(p);
						        getGlassPane().setVisible(false);
						        if(lastDropDescriptor!=null){
					    			lastDropDescriptor.updateDropEffect(null);
					    			getGlassPane().setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
					    		}
						        getGlassPane().setDragEffect(null);
						        
						        Component dropComp=getDropTargetAt(ptOnScreen, contentPane, getContentPaneRecOnScreen(), dragS);
						        
						        if(dropComp!=null){
						        	p=(Point)ptOnScreen.clone();
						        	SwingUtilities.convertPointFromScreen(p, dropComp);
						        	DropEvent dropEv=new DropEvent(dropComp,p,dragS.getDragDescriptor().getDraggedContent(),dragS);
						        	((DropTarget)dropComp).getDropDescriptor().fireContentDropped(dropEv);
						        	dragS.getDragDescriptor().dropPerformed(dropEv);
						        }
						        else{
						        	dragS.getDragDescriptor().dropUnsupported();
						        }
						        dragS.getDragDescriptor().setDraggedContent(null);
								dragS.getDragDescriptor().setDragEffect(null);
							}
						}
					}
				}
				else if(dragS.getDragDescriptor()!=null){
					getGlassPane().setVisible(false);
					getGlassPane().setDragEffect(null);
					dragS.getDragDescriptor().setDraggedContent(null);
					dragS.getDragDescriptor().setDragEffect(null);
				}
				draggedEvent=null;
			}
			
			public void mouseDragged(MouseEvent e){
				if(((Component)dragS).isEnabled()){
					if((e.getModifiersEx() & MouseEvent.BUTTON1_DOWN_MASK)==MouseEvent.BUTTON1_DOWN_MASK){
						if(draggedEvent!=null){
							Point pt=(Point) e.getPoint().clone();
							SwingUtilities.convertPointToScreen(pt, e.getComponent());
							SwingUtilities.convertPointFromScreen(pt, getGlassPane());
			        		getGlassPane().setPoint(pt);
							
							
							Point ptOnScreen=(Point)e.getPoint().clone();
			        		SwingUtilities.convertPointToScreen(ptOnScreen, e.getComponent());
			        		
			        		scrollIfNeeded(getContentPane(), ptOnScreen, getContentPaneRecOnScreen());
			        		
			        		Component dropComp=getDropTargetAt(ptOnScreen, getContentPane(), getContentPaneRecOnScreen(),dragS);
			        		if(dropComp!=null){
				        		if(lastDropDescriptor!=null){
				        			if(lastDropDescriptor!=((DropTarget)dropComp).getDropDescriptor()){
				        				lastDropDescriptor.updateDropEffect(null);
				        			}
				        		}
			        		}
			        		else if(lastDropDescriptor!=null)
			        			lastDropDescriptor.updateDropEffect(null);
			        		
			        		lastDropDescriptor=dropComp==null ? null:((DropTarget)dropComp).getDropDescriptor();
			        		
			        		if(lastDropDescriptor!=null){
			        			pt=(Point)ptOnScreen.clone();
			        			SwingUtilities.convertPointFromScreen(pt, dropComp);
			        			
			        			getGlassPane().setCursor(Cursors.allowedCursor);
			        			lastDropDescriptor.updateDropEffect(new DropEvent(dropComp,pt,dragS.getDragDescriptor().getDraggedContent(),dragS));
			        		}
			        		else{
			        			getGlassPane().setCursor(Cursors.forbiddenCursor);
			        		}
						}
					}
				}
			}
			
			private Rectangle getContentPaneRecOnScreen(){
        		Rectangle recOnScreen=(Rectangle)getContentPane().getBounds().clone();
				Point contentPaneLocOnScreen=recOnScreen.getLocation();
				SwingUtilities.convertPointToScreen(contentPaneLocOnScreen, contentPane.getParent());
				recOnScreen.setLocation(contentPaneLocOnScreen);
        		return recOnScreen;
        	}
			
			private Container getContentPane(){
				if(contentPane==null){
					contentPane=((JFrame)(SwingUtilities.getWindowAncestor((Component)dragS))).getContentPane();
				}
				return contentPane;
			}
			
			private MyGlassPane getGlassPane(){
				if(glassPane==null){
					glassPane=(MyGlassPane)((JFrame)(SwingUtilities.getWindowAncestor((Component)dragS))).getGlassPane();
				}
				return glassPane;
			}
		});
		MouseAdapter mouseListener=dndMap.get(dragSource);
		dragS.getDragDescriptor().getComponentSourceOfDrag().addMouseListener(mouseListener);
		dragS.getDragDescriptor().getComponentSourceOfDrag().addMouseMotionListener(mouseListener);
	}
	
	private static Container getContentPane(Component component){
		return ((JFrame)(SwingUtilities.getWindowAncestor(component))).getContentPane();
	}
	
	
	/**
	 * This method is not meant to be used by application developers.
	 * @param ptOnScreen the coordinates  of the point based on the screen coordinates system.
	 * @param accessibleRecOnScreen the rectangle representing the showing area of <i>container</i> based on screen coordinates system.
	 * @param container the container on wich this method will search for a drop target.
	 * @param droppedContent the content to drop.
	 * @return the DropTarget that contains <i>ptOnScreen</i> on its showingArea and on wich 
	 * {@link DropDescriptor#isDropEventSupported(DropEvent)}==true.<br/>
	 *  If theres no DropTarget that matchs these creterias returns null.
	 */
	protected static Component getDropTargetAt(Point ptOnScreen, Container container, Rectangle accessibleRecOnScreen, DragSource dragS){
		for(Component comp:container.getComponents()){
			if(comp.isShowing()){
				//comp bounds on container coordinates system
				Rectangle r=(Rectangle)comp.getBounds().clone();
				Point pt=r.getLocation();
				//***************************************
				
				//convert to screen coordinate system
				SwingUtilities.convertPointToScreen(pt, container);
				r.setLocation(pt);
				//*************************
				
				Rectangle.intersect(accessibleRecOnScreen, r, r);
				if(!r.isEmpty()){
					if(r.contains(ptOnScreen)){
						//******************************************ici les conditions du drop********************************* 
						if(comp instanceof DropTarget){
							if(((DropTarget)comp).getDropDescriptor()!=null){
								pt=(Point)ptOnScreen.clone();
								SwingUtilities.convertPointFromScreen(pt, comp);
								
								if(((DropTarget)comp).getDropDescriptor().isDropEventSupported(
												new DropEvent(comp, pt, dragS.getDragDescriptor().getDraggedContent(),dragS))){
									return comp;
								}
							}
						}
						//*********************************************************************************************
					
						if(comp instanceof Container){
							Component c=getDropTargetAt(ptOnScreen, (Container)comp,r,dragS);
							if(c!=null)
								return c;
						}
					}
				}
			}
		}
		
		return null;
	}
	
	/**
	 * this method will check if the mouse cursor is pointed close to the borders of any viewport (that must be a <u>descendant</u> of 
	 * comp.getParent().getParent()) and if so does the needed scrolling.<br/>
	 * the scrolling will be performed only on the <u>showing</u> areas wich are close to the viewport borders.<br/>
	 * <u>close</u> is determined by {@link #MARGIN_OF_SCROLLING}
	 */
	private static boolean scrollIfNeeded(Component comp, Point ptOnScreen, Rectangle visibleRecOnScreen){
		if(comp.isShowing()){
			Rectangle r=new Rectangle();
			//comp bounds on container coordinates system
			Rectangle compBounds=(Rectangle)comp.getBounds().clone();
			Point pt=compBounds.getLocation();
			//***************************************
			
			//convert to screen coordinate system
			SwingUtilities.convertPointToScreen(pt, comp.getParent());
			compBounds.setLocation(pt);
			//*************************
			
			
			Rectangle.intersect(visibleRecOnScreen, compBounds, r);
			if(r.contains(ptOnScreen)){//recursivity must be applied inside this bloc.
				if(comp.getParent() instanceof JViewport){
					Rectangle rec=calculateRecToScrollInto(ptOnScreen, r);
					if(rec!=null){
						Rectangle.intersect(compBounds, rec, rec);
						if(!rec.isEmpty()){
							pt=rec.getLocation();
							//convert to screen coordinate system
							SwingUtilities.convertPointFromScreen(pt, comp);
							rec.setLocation(pt);
							
							((JComponent)comp).scrollRectToVisible(rec);
							return true;
						}
					}
				}
				if(comp instanceof Container){
					for(Component c:((Container)comp).getComponents()){
						if(scrollIfNeeded(c, ptOnScreen, r))
							return true;
					}
				}
			}
		}
		return false;
	}
	
	/**
	 * pt is supposed to be inside rec, otherwise this method will return wrong results.
	 * @param pt
	 * @param rec
	 * @return null if no scrolling is needed.
	 */
	private static Rectangle calculateRecToScrollInto(Point pt, Rectangle rec){
		if(pt.y-rec.y<MARGIN_OF_SCROLLING){
			if(pt.x-rec.x<MARGIN_OF_SCROLLING){
				//north west
				return new Rectangle(rec.x-MARGIN_OF_SCROLLING,rec.y-MARGIN_OF_SCROLLING,MARGIN_OF_SCROLLING,MARGIN_OF_SCROLLING);
			}
			else if(rec.x+rec.width-pt.x<MARGIN_OF_SCROLLING)
				//north east
				return new Rectangle(rec.x+rec.width,rec.y-MARGIN_OF_SCROLLING,MARGIN_OF_SCROLLING,MARGIN_OF_SCROLLING);
			//north
			return new Rectangle(pt.x,rec.y-MARGIN_OF_SCROLLING,MARGIN_OF_SCROLLING,MARGIN_OF_SCROLLING);
		}
		if(rec.y+rec.height-pt.y<MARGIN_OF_SCROLLING){
			if(pt.x-rec.x<MARGIN_OF_SCROLLING){
				//south west
				return new Rectangle(rec.x-MARGIN_OF_SCROLLING,rec.y+rec.height,MARGIN_OF_SCROLLING,MARGIN_OF_SCROLLING);
			}
			else if(rec.x+rec.width-pt.x<MARGIN_OF_SCROLLING)
				//south east
				return new Rectangle(rec.x+rec.width,rec.y+rec.height,MARGIN_OF_SCROLLING,MARGIN_OF_SCROLLING);
			//south
			return new Rectangle(pt.x,rec.y+rec.height,MARGIN_OF_SCROLLING,MARGIN_OF_SCROLLING);
		}
		if(pt.x-rec.x<MARGIN_OF_SCROLLING){
			//west
			return new Rectangle(rec.x-MARGIN_OF_SCROLLING,pt.y,MARGIN_OF_SCROLLING,MARGIN_OF_SCROLLING);
		}
		if(rec.x+rec.width-pt.x<MARGIN_OF_SCROLLING)
			//east
			return new Rectangle(rec.x+rec.width,pt.y,MARGIN_OF_SCROLLING,MARGIN_OF_SCROLLING);
		
		return null;
	}
	
	/**
	 * @param component
	 * @param pointOnScreen
	 * @return
	 * 		true if the <i>component</i> contains <i>pointOnScreen</i> on its <u>showing</u> area, otherwise returns false.<br/>
	 * 		the showing area is the visible part of the component. 
	 */
	public static boolean isPointOnShowingAreaOfComponent(Component component, Point pointOnScreen){
		if(component.isShowing()){
			Point pt=(Point)pointOnScreen.clone();
			SwingUtilities.convertPointFromScreen(pt, component);
			if(component.contains(pt)){
				if(component==getContentPane(component))
					return true;
				else
					return isPointOnShowingAreaOfComponent(component.getParent(), pointOnScreen);
			}
		}
		return false;
	} 
	
	
	private static Map<Component, MouseAdapter> getDndMap() {
		if(dndMap==null){
			dndMap=new HashMap<Component, MouseAdapter>();
		}
		return dndMap;
	}
	
}
