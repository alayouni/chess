package components.dragndrop;

import java.awt.Component;
import java.awt.Point;


public class DropEvent {
	private Point location=null;
	private Object droppedContent=null;
	private Component dropTarget=null;
	private DragSource dragSource=null;
	
	/**
	 * @param dropTarget the component on which the drop has occurred.
	 * @param location the location based on dropTarget coordinate System.
	 * @param droppedContent
	 * @param dragSource 
	 */
	public DropEvent(Component dropTarget, Point location, Object droppedContent, DragSource dragSource){
		this.location=new Point(location.x,location.y);
		this.droppedContent=droppedContent;
		this.dropTarget=dropTarget;
		this.dragSource=dragSource;
	}
	
	/**
	 * @return The component being dragged.
	 */
	public DragSource getDragSource() {
		return dragSource;
	}

	/** 
	 * @return the location based on {@link #getDropTarget()} coordinate System.
	 */
	public Point getLocation() {
		return location;
	}
	
	/**
	 * @return {@link #getLocation()}.x
	 */
	public int getX(){
		return getLocation().x;
	}
	
	/**
	 * @return {@link #getLocation()}.y
	 */
	public int getY(){
		return getLocation().y;
	}
	
	/**
	 * 
	 * @return the droppedContent.
	 */
	public Object getDroppedContent() {
		return droppedContent;
	}
	
	
	/**
	 * @return the component on which the drop has occurred.
	 */
	public Component getDropTarget() {
		return dropTarget;
	}
	
}
