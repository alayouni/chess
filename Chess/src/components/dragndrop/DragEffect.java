package components.dragndrop;

import java.awt.Graphics;

/**
 * Describes the dragging graphical effects to be drawn on the glass pane.  
 */
public interface DragEffect {
	/**
	 * Describes the dragging graphical effects to be drawn on the glass pane.<br/>
	 * An implementation of this method should not deal with transparency, {@link MyGlassPane} will handle it.<br/>
	 * An implementation of this method must consider the mouse cursor as the origin of the coordinate system i.e., mouse cursor coordinates=(0,0).
	 * Graphical effects with negative coordinates will be taken in consideration, as far as they fit inside the ancestor window. 
	 * @param g
	 */
	public void paintEffect(Graphics g);
}
