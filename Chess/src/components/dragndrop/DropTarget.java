package components.dragndrop;


/**
 * Each component that is intended to handle drop Events must implement this interface.
 * @see {@link DragSource}
 */
public interface DropTarget {
	/**
	 * @return an instance of {@link DropDescriptor} if it was previously installed on this component by the mean of
	 * {@link #setDropDescriptor(DropDescriptor)}.<br/>
	 * 	If no {@link DropDescriptor} were installed returns null.
	 */
	public DropDescriptor getDropDescriptor();
	
	
	/**
	 * Installs a {@link DropDescriptor} on this component.<br/><br/>
	 * 
	 * <b>Note: </b> although {@link DragSource#setDragDescriptor(DragDescriptor)} needs an other treatment to take effect,
	 *  this method doesn't.    
	 * @param dropDescriptor the DropDescriptor to be installed.
	 * @see #getDropDescriptor()
	 */
	public void setDropDescriptor(DropDescriptor dropDescriptor);
}
