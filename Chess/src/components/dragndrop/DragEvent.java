package components.dragndrop;
import java.awt.Component;
import java.awt.Point;



public class DragEvent {
	private Point location=null;
	private Component source=null;
	
	/**
	 * @param source the component on which the drag has occurred.
	 * @param location the location based on source coordinate System.
	 */
	public DragEvent(Component source, Point location){
		this.location=location;
		this.source=source;
	}
	
	/** 
	 * @return the location where the drag event has occurred, based on {@link #getSource()} coordinate System.
	 */
	public Point getLocation() {
		return location;
	}
	
	/**
	 * @return {@link #getLocation()}.x
	 */
	public int getX(){
		return getLocation().x;
	}
	
	/**
	 * @return {@link #getLocation()}.y
	 */
	public int getY(){
		return getLocation().y;
	}
	
	/**
	 * @return the component on which the drag event has occurred.
	 */
	public Component getSource() {
		return source;
	}

}




