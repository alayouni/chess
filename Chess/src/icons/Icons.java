package icons;

import javax.swing.ImageIcon;

public interface Icons {
	String iconsRep="/icons/";
	String whiteRep=iconsRep+"white/";
	String blackRep=iconsRep+"black/";
	
	ImageIcon logo=new ImageIcon(Icons.class.getResource(iconsRep+"logo.png"));
	ImageIcon contour=new ImageIcon(Icons.class.getResource(iconsRep+"contour.jpg"));
	ImageIcon start=new ImageIcon(Icons.class.getResource(iconsRep+"start.jpg"));
	
	ImageIcon pawnWhite=new ImageIcon(Icons.class.getResource(whiteRep+"pawn.png"));
	ImageIcon bishopWhite=new ImageIcon(Icons.class.getResource(whiteRep+"bishop.png"));
	ImageIcon knightWhite=new ImageIcon(Icons.class.getResource(whiteRep+"knight.png"));
	ImageIcon towerWhite=new ImageIcon(Icons.class.getResource(whiteRep+"tower.png"));
	ImageIcon queenWhite=new ImageIcon(Icons.class.getResource(whiteRep+"queen.png"));
	ImageIcon kingWhite=new ImageIcon(Icons.class.getResource(whiteRep+"king.png"));
	
	ImageIcon pawnBlack=new ImageIcon(Icons.class.getResource(blackRep+"pawn.png"));
	ImageIcon bishopBlack=new ImageIcon(Icons.class.getResource(blackRep+"bishop.png"));
	ImageIcon knightBlack=new ImageIcon(Icons.class.getResource(blackRep+"knight.png"));
	ImageIcon towerBlack=new ImageIcon(Icons.class.getResource(blackRep+"tower.png"));
	ImageIcon queenBlack=new ImageIcon(Icons.class.getResource(blackRep+"queen.png"));
	ImageIcon kingBlack=new ImageIcon(Icons.class.getResource(blackRep+"king.png"));

}
