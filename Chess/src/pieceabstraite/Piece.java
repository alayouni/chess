package pieceabstraite;
import icons.Icons;

import java.awt.Graphics;
import java.util.Vector;

import javax.swing.JLabel;

import casepack.Cas;


public abstract class Piece {
	public static final short roi=0;
	public static final short reine=1;
	public static final short fou=2;
	public static final short cheval=3;
	public static final short tour=4;
	public static final short pion=5;
	private static JLabel label=null;
	public short ind;
	public Cas pos;
	public short coul;
	public abstract boolean validmove(Cas casef);
	public abstract Vector<Cas> validmoves();
	public Piece(){}
	
	public final static short abs(int z){
		if(z<0)
			return (short)(-z);
		return (short)z;
	}
	
	public final static boolean memeligne(Cas casei,Cas casef){
		if(casei.l==casef.l||casei.c==casef.c)
			return true;
		return false;
	}
	
	public final static boolean memediagonal(Cas casei,Cas casef){
		if(abs(casei.l-casef.l)==abs(casef.c-casei.c))
			return true;
		return false;
	}
	
	public static void drawPiece(Graphics g, int x, int y, short piece, boolean white){
		setPiece(piece, white);
		g.translate(x, y);
		getLabel().paint(g);
		g.translate(-x, -y);
	}
	
	private static void setPiece(short piece, boolean white){
		switch (piece) {
		case roi:
			getLabel().setIcon(white?Icons.kingWhite:Icons.kingBlack);
			break;
		case reine:
			getLabel().setIcon(white?Icons.queenWhite:Icons.queenBlack);
			break;
		case fou:
			getLabel().setIcon(white?Icons.bishopWhite:Icons.bishopBlack);
			break;
		case cheval:
			getLabel().setIcon(white?Icons.knightWhite:Icons.knightBlack);
			break;
		case tour:
			getLabel().setIcon(white?Icons.towerWhite:Icons.towerBlack);
			break;
		case pion:
			getLabel().setIcon(white?Icons.pawnWhite:Icons.pawnBlack);
			break;
		default:
			getLabel().setIcon(null);
			break;
		}
	}
	
	private static JLabel getLabel(){
		if(label==null){
			label=new JLabel();
			label.setSize(Icons.bishopBlack.getIconWidth(), Icons.bishopBlack.getIconHeight());
			label.setLocation(0, 0);
		}
		return label;
	}
}
