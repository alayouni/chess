package cursors;

import java.awt.Cursor;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;

import javax.swing.ImageIcon;

public interface Cursors {
	public final Image forbiddenImg=new ImageIcon(Object.class.getResource("/cursors/forbidden.png")).getImage();
	public final Cursor forbiddenCursor=Toolkit.getDefaultToolkit().createCustomCursor(
								forbiddenImg, new Point(16,16), "ahmed.cursors.forbidden");
	
	public final Image allowedImg=new ImageIcon(Object.class.getResource("/cursors/allowed.png")).getImage();
	public final Cursor allowedCursor=Toolkit.getDefaultToolkit().createCustomCursor(
			allowedImg, new Point(16,16), "ahmed.cursors.allowed");
}
