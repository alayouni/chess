package ro93a;
import interfacegrafic.Chess;
import interfacegrafic.ChoixPiece;

import java.util.ArrayList;
import java.util.Vector;
import java.util.prefs.Preferences;

import javax.swing.SwingUtilities;

import pieceabstraite.Piece;
import pieces.Cheval;
import pieces.Fou;
import pieces.Pion;
import pieces.Reine;
import pieces.Roi;
import pieces.Tour;
import casepack.Cas;
public class Echiquier {
	/**
	 * first index between 0(white) and 1(black).
	 * second index for the 6 different types pf pieces ==> between 0 and 5
	 * 3rd index for occurences of each piece type.
	 */
	public ArrayList<ArrayList<ArrayList<Piece>>> pieces=new ArrayList<ArrayList<ArrayList<Piece>>>();
	public Cas selection=null;
	short turn=0;
	Pion cptpion=null;
	public Chess apercu=null;
	Clocks horloge=null;
	private static Preferences node=null;
	
	@SuppressWarnings("serial")
	class MyArrayList extends ArrayList<ArrayList<ArrayList<ArrayList<Cas>>>>{
		public boolean add(ArrayList<ArrayList<ArrayList<Piece>>> pieces){
			ArrayList<ArrayList<ArrayList<Cas>>> poss=new ArrayList<ArrayList<ArrayList<Cas>>>();
			for(int i=0;i<2;i++){
				poss.add(new ArrayList<ArrayList<Cas>>());
				for(int j=0;j<6;j++){
					poss.get(i).add(new ArrayList<Cas>());
					for(int k=0;k<pieces.get(i).get(j).size();k++){
						Piece pc=pieces.get(i).get(j).get(k);
						poss.get(i).get(j).add(new Cas(pc.pos));
					}
				}
			}
			super.add(poss);
			return true;
		}
	}
	
	
	private MyArrayList etats=new MyArrayList(); 
	private ArrayList<Integer> occurences=new ArrayList<Integer>();
	public Echiquier(int timer){
		for(short i=0;i<2;i++){
			pieces.add(new ArrayList<ArrayList<Piece>>());
			pieces.get(i).add(new ArrayList<Piece>());
			pieces.get(i).get(Piece.roi).add(new Roi(i));
			
			pieces.get(i).add(new ArrayList<Piece>());
			pieces.get(i).get(Piece.reine).add(new Reine(i));
			
			pieces.get(i).add(new ArrayList<Piece>());
			pieces.get(i).add(new ArrayList<Piece>());
			pieces.get(i).add(new ArrayList<Piece>());
			for(int j=0;j<2;j++){
				pieces.get(i).get(Piece.fou).add(new Fou(i));
				pieces.get(i).get(Piece.cheval).add(new Cheval(i));
				pieces.get(i).get(Piece.tour).add(new Tour(i));
			}
			
			pieces.get(i).add(new ArrayList<Piece>());
			for(int j=0;j<8;j++){
				pieces.get(i).get(Piece.pion).add(new Pion(i));
			}
			
			// positionnement des pions
			for(short j=0;j<8;j++){
				Pion pp=(Pion)(pieces.get(i).get(Piece.pion).get(j));
				pp.pos=new Cas((short)(-5*i+6),j);
			}
			
			//positonnement roi,reine ... tour
			for(short j=0;j<5;j++){
				Piece pc=(Piece)(pieces.get(i).get(j).get(0));
				pc.pos=new Cas((short)(-7*i+7),(short)(-j+4));
			}
			
			//positionnement tour, cheval, fou
			for(short j=2;j<5;j++){
				Piece pc=(Piece)(pieces.get(i).get(j).get(1));
				pc.pos=new Cas(-7*i+7,j+3);
			}
		}
		etats.add(pieces);
		occurences.add(new Integer(1));
		apercu=new Chess(this);
		horloge=new Clocks(turn,timer);
		horloge.start();
		
	}
	public static final String TIMER_KEY="timer";
	public static void main(String[] args) {
		int timer=0;
		timer=getNode().getInt(TIMER_KEY, 5);
		new Echiquier(timer);
	}
	
	public Piece contenucase(Cas pos){
		for(int i=0;i<2;i++)
			for(int j=0;j<6;j++)
				for(int k=0;k<pieces.get(i).get(j).size();k++){
					Piece pc=(Piece)(pieces.get(i).get(j).get(k));
					if(pc.pos.egal(pos))
						return pc;
				}
		return null;
	}
	
	public Piece analyser(Cas evmt){
		Piece pc;
		if((pc=contenucase(evmt))!=null){
			if(pc.coul==turn){
				apercu.select(evmt);
				selection=new Cas(evmt);
				return pc;
			}
			else if (selection!=null){
				//capture si le mouvement est valide
				Piece pci=contenucase(selection);
				if(validmove(evmt)>0){
					pci.pos=new Cas(evmt);
					pieces.get(pc.coul).get(pc.ind).remove(pc);
					if(!ischeck()){
						etats.clear();
						occurences.clear();
						pci.pos=new Cas(selection);
						//s'il s'agit d'une suppression de tour annuler le rokage
						if(pc instanceof Tour){
							Roi ro=(Roi)(pieces.get(pc.coul).get(Piece.roi).get(0));
							if(pc.pos.egal(new Cas(ro.pos.l,0)))
								ro.queenside=false;
							else if(pc.pos.egal(new Cas(ro.pos.l,7)))
								ro.kingside=false;
						}
						//*****************************************************
						
						
						//s'il s'agit d'un mouvement de roi annulation du rokage
						if(pci instanceof Roi){
							((Roi)(pci)).kingside=false;
							((Roi)(pci)).queenside=false;
						}
						//*******************************************************
						
						//s'il s'agit d'un mouvement de tour annulation de rokage
						else if(pci instanceof Tour){
							Roi ro=(Roi)(pieces.get(pci.coul).get(Piece.roi).get(0));
							if(selection.egal(new Cas(ro.pos.l,0)))
								ro.queenside=false;
							else if(selection.egal(new Cas(ro.pos.l,7)))
								ro.kingside=false;
						}
						//********************************************************
						
						move(pci,evmt);
						if(pci instanceof Pion){
							if(pci.pos.l==((Pion)pci).dernier){
								apercu.setEnabled(false);
								pieces.get(pci.coul).get(pci.ind).remove(pci);
								ChoixPiece chp=new ChoixPiece(apercu,pci,this);
								chp.setLocation(apercu.getLocation().x+691, apercu.getLocation().y+230);
							}
							else{
								switchclock();
								etats.add(pieces);
								occurences.add(new Integer(1));
							}
						}
						else{
							switchclock();
							etats.add(pieces);
							occurences.add(new Integer(1));
						}
						if(ischeck())
							apercu.kech(pieces.get(turn).get(Piece.roi).get(0).pos);
						
						//*****************v�rifier si les nouveaux pieces forment un draw**********
						if(pieces.get(0).get(Piece.reine).size()==0&&pieces.get(1).get(Piece.reine).size()==0){
							if(pieces.get(0).get(Piece.tour).size()==0&&pieces.get(1).get(Piece.tour).size()==0)
								if(pieces.get(0).get(Piece.pion).size()==0&&pieces.get(1).get(Piece.pion).size()==0){
									boolean b=true;
									for(int i=0;i<2&&b;i++){
										int coul=-1;
										for(int j=0;j<pieces.get(i).get(Piece.fou).size()&&b;j++){
											Piece pcc=((Piece)(pieces.get(i).get(Piece.fou).get(j)));
											if(coul!=-1&&coul!=pcc.coul)
												b=false;
											coul=pcc.coul;
										}
									}
									if(b){
										b=(pieces.get(0).get(Piece.fou).size()==0)&&(pieces.get(0).get(Piece.cheval).size()<2);
										b=b||(pieces.get(0).get(Piece.cheval).size()==0);
										if(b){
											b=(pieces.get(1).get(Piece.fou).size()==0)&&(pieces.get(1).get(Piece.cheval).size()<2);
											b=b||(pieces.get(1).get(Piece.cheval).size()==0);
											if(b){
												horloge.stopClocks();
												apercu.message.setText("Insufficient pieces, Draw\n\n1/2  -  1/2");
												apercu.getBoard().setEnabled(false);
											}
										}
									}
								}
						}
						//**************************************************************************
					}
					else{
						pieces.get(pc.coul).get(pc.ind).add(pc);
						pci.pos=new Cas(selection);
					}
				}
			}
		}
		else if(selection!=null){
			//verifier la validit� du mvt
			Piece pci=contenucase(selection);
			int valid;
			if((valid=validmove(evmt))>0){
				if(valid==1){
					pci.pos=new Cas(evmt);
					if(!ischeck()){
						pci.pos=new Cas(selection);
					
						//s'il s'agit d'un mouvement de roi annuler le rokage
						if(pci instanceof Roi){
							((Roi)(pci)).kingside=false;
							((Roi)(pci)).queenside=false;
						}
						//****************************************************
					
						//s'il s'agit d'un mouvement de tour annuler le rokage 
						else if(pci instanceof Tour){
							Roi ro=(Roi)(pieces.get(pci.coul).get(Piece.roi).get(0));
							if(selection.egal(new Cas(ro.pos.l,0)))
								ro.queenside=false;
							else if(selection.egal(new Cas(ro.pos.l,7)))
								ro.kingside=false;	
						}
						//*******************************************************
						int dif=Piece.abs(selection.l-evmt.l);//servira dans la permission du capteur de pion
						move(pci,evmt);
						//s'il s'agit d'une permission de capteur de pion 
						boolean b=true,mvtpion=false;
						if(pci instanceof Pion){
							mvtpion=true;
							if(dif==2){
								cptpion=new Pion((short)1);
								cptpion=(Pion)pci;
							}
							else if(pci.pos.l==((Pion)pci).dernier){
								apercu.setEnabled(false);
								pieces.get(pci.coul).get(pci.ind).remove(pci);
								ChoixPiece chp=new ChoixPiece(apercu,pci,this);
								chp.setLocation(apercu.getLocation().x+691, apercu.getLocation().y+230);
								b=false;
							}
						}
						//**********************************************
						if(b){
							switchclock();
							if(mvtpion){
								etats.add(pieces);
								occurences.add(new Integer(1));
							}
							else{
								//verifier s'il s'agit d'un draw de repetition et agir sur etats sinon
								int r;
								if((r=repeated())>-1){
									int oc=((Integer)(occurences.get(r))).intValue();
									if(oc<2){
										occurences.set(r,new Integer(2));
									}
									else{
										horloge.stopClocks();
										apercu.message.setText("Draw by repetition 3 times !\n\n1/2  -  1/2");
										apercu.getBoard().setEnabled(false);
									
									}
								}
								else{
									etats.add(pieces);
									occurences.add(new Integer(1));
								}
							}
						}
					}
				
					else{
						pci.pos=new Cas(selection);
					}
				}
				
				//*******************rockage***********************
				else if(valid==2){
					((Roi)pci).queenside=false;
					((Roi)pci).kingside=false;
					if(evmt.c==2){
						Tour t=(Tour)contenucase(new Cas(pci.pos.l,0));
						move(t,new Cas(pci.pos.l,3));
					}
					else{
						Tour t=(Tour)contenucase(new Cas(pci.pos.l,7));
						move(t,new Cas(pci.pos.l,5));
					}
					move(pci,evmt);
					turn=(short)(1-turn);
					switchclock();
					etats.add(pieces);
					occurences.add(new Integer(1));
				}
				//************************************************
				
				//*************capteur de pion*********************
				else{
					pieces.get(1-turn).get(Piece.pion).remove(cptpion);
					pci.pos=new Cas(evmt);
					if(!ischeck()){
						pci.pos=new Cas(selection);
						apercu.updateView();
						move(pci,evmt);
						switchclock();
						etats.clear();
						occurences.clear();
						etats.add(pieces);
						occurences.add(new Integer(1));
					}
					else{
						pieces.get(1-turn).get(Piece.pion).add(cptpion);
						pci.pos=new Cas(selection);
					}
				}
				//**************************************************
			}
		}
		return null;
	}
	
	
	public void entrerpiece(Piece pc){
		etats.clear();
		occurences.clear();
		pieces.get(pc.coul).get(pc.ind).add(pc);
		apercu.updateView();
		apercu.setEnabled(true);
		switchclock();
		etats.add(pieces);
		occurences.add(new Integer(1));
		//*****************v�rifier si les nouveaux pieces forment un draw**********
		if(pieces.get(0).get(Piece.reine).size()==0&&pieces.get(1).get(Piece.reine).size()==0){
			if(pieces.get(0).get(Piece.tour).size()==0&&pieces.get(1).get(Piece.tour).size()==0)
				if(pieces.get(0).get(Piece.pion).size()==0&&pieces.get(1).get(Piece.pion).size()==0){
					boolean b=true;
					for(int i=0;i<2&&b;i++){
						int coul=-1;
						for(int j=0;j<pieces.get(i).get(Piece.fou).size()&&b;j++){
							Piece pcc=((Piece)(pieces.get(i).get(Piece.fou).get(j)));
							if(coul!=-1&&coul!=pcc.coul)
								b=false;
							coul=pcc.coul;
						}
					}
					if(b){
						b=(pieces.get(0).get(Piece.fou).size()==0)&&(pieces.get(0).get(Piece.cheval).size()<2);
						b=b||(pieces.get(0).get(Piece.cheval).size()==0);
						if(b){
							b=(pieces.get(1).get(Piece.fou).size()==0)&&(pieces.get(1).get(Piece.cheval).size()<2);
							b=b||(pieces.get(1).get(Piece.cheval).size()==0);
							if(b){
								horloge.stopClocks();
								apercu.message.setText("Insufficient pieces, Draw\n\n1/2  -  1/2");
								apercu.getBoard().setEnabled(false);
							}
						}
					}
				}
		}
		//**************************************************************************
	}
	
	
	
	public void switchclock(){
		if(!canmove()){
			horloge.stopClocks();
			if(ischeck()){
				String winner;
				if(turn==0)
					winner=new String("Black");
				else
					winner=new String("White");
				apercu.message.setText("Checkmate !\n\n"+winner+" wins");
				apercu.kech(pieces.get(turn).get(Piece.roi).get(0).pos);
				apercu.getBoard().setEnabled(false);
			}
			else{
				apercu.message.setText("Draw by stalemate\n\n1/2  -  1/2");
				apercu.getBoard().setEnabled(false);
			}
		}
		else{
			horloge.switchClocks();
			if(ischeck())
				apercu.kech(pieces.get(turn).get(Piece.roi).get(0).pos);
			
		}
	}
	
	public void move(Piece pc,Cas casef){
		selection=null;
		apercu.move(pc.pos,casef);
		pc.pos=new Cas(casef);
		turn=(short)(1-turn);
		cptpion =new Pion((short)1);
		cptpion=null;
	}
	
	public static final short sup(short i,short j){
		if(i>j)
			return i;
		return j;
	}
	
	public static final short inf(short i,short j){
		if(i>j)
			return j;
		return i;
	}
	
	
	public boolean lignevide(Cas casei,Cas casef){
		if(casei.l==casef.l){
			for(short j=(short)(inf(casef.c,casei.c)+1);j<sup(casef.c,casei.c);j++)
				if(contenucase(new Cas(casei.l,j))!=null)
					return false;
		}
		for(short j=(short)(inf(casef.l,casei.l)+1);j<sup(casef.l,casei.l);j++)
			if(contenucase(new Cas(j,casei.c))!=null)
				return false;
		return true;
	}
	
	public boolean diagonalvide(Cas casei,Cas casef){
		short p1=(short)(casef.l-casei.l),p2=(short)(casef.c-casei.c);
		p1/=Piece.abs(p1);
		p2/=Piece.abs(p2);
		for(short i=(short)(casei.l+p1),j=(short)(casei.c+p2);i!=casef.l;i+=p1,j+=p2){
			if(contenucase(new Cas(i,j))!=null){
				return false;
			}
		}
		return true;
	}
	
	public boolean ischeck(){
		Cas casroi=new Cas(((Roi)(pieces.get(turn).get(Piece.roi).get(0))).pos);
		for(short i=0;i<6;i++){
			for(short j=0;j<pieces.get(1-turn).get(i).size();j++){
				Piece pc=(Piece)(pieces.get(1-turn).get(i).get(j));
				if(i==Piece.pion){
					if(Piece.abs(casroi.l-pc.pos.l)==1&&Piece.abs(casroi.c-pc.pos.c)==1)
						if(pc.validmove(new Cas(casroi.l,pc.pos.c)))
							return true;
				}
				else if(i==Piece.tour){
					if(pc.validmove(casroi))
						if(lignevide(pc.pos,casroi))
							return true;
				}
				else if(i==Piece.cheval){
					if(pc.validmove(casroi))
						return true;
				}
				else if(i==Piece.fou){
					if(pc.validmove(casroi))
						if(diagonalvide(pc.pos,casroi))
							return true;
				}
				else if(i==Piece.reine){
					if(pc.validmove(casroi)){
						if(Piece.memediagonal(pc.pos,casroi)){
							if(diagonalvide(pc.pos,casroi))
								return true;
						}
						else if(lignevide(pc.pos,casroi))
							return true;
					}
				}
				else if(i==Piece.roi){
					if(pc.validmove(casroi))
						return true;
				}
			}
		}
		return false;
	}
	
	
	public int validmove(Cas casef){
		Piece pc=contenucase(selection);
		Piece pcf=contenucase(casef);
		if(pcf!=null){
			if(pcf.coul==pc.coul)
				return 0;
		}
		
		if(pc instanceof Roi){
			if(pc.validmove(casef))
				return 1;
			else if(validrokage((Roi)pc,casef))
				return 2;
			
		}
		else if(pc instanceof Pion){
			if(pc.validmove(casef)){
				short p=(short)(casef.l-selection.l);
				p/=Piece.abs(p);
				if(lignevide(selection,new Cas(casef.l+p,casef.c))){
					return 1;
				}
			}
			else if(Piece.memediagonal(selection,casef)&&casef.l-selection.l==2*pc.coul-1){
				if(pcf!=null)
					return 1;
				if(cptpion!=null){
					if(Piece.abs(cptpion.pos.c-selection.c)==1)
						if(cptpion.pos.l==selection.l){
							short l=(short)(2*turn-1+selection.l);
							if(casef.egal(new Cas(l,cptpion.pos.c))){
								return 3;
							}
							
						}
				}	
			}
		}
		else if (pc!=null){
			if(pc.validmove(casef)){
				if(Piece.memediagonal(selection,casef)){
					if(diagonalvide(selection,casef)){
						return 1;
					}
				}
				else if(Piece.memeligne(selection,casef)){
					if(lignevide(selection,casef)){
						return 1;
					}
				}
				else{
					return 1;
				}
			}
		}
		return 0;
	}
	
	public boolean validrokage(Roi ro,Cas casef){
		if(ro.coul==0){
			if(ro.pos.egal(new Cas(7,4))){
				if(casef.egal(new Cas(7,2))){
					if(ro.queenside){
						if(lignevide(new Cas(7,0),ro.pos))
							if(!ischeck()){
								ro.pos=new Cas(7,3);
								if(!ischeck()){
									ro.pos=new Cas(7,2);
									if(!ischeck()){
										ro.pos=new Cas(7,4);
										return true;
									}
								}
								ro.pos=new Cas(7,4);
							}
					}
				}
				else if(casef.egal(new Cas(7,6))){
					if(ro.kingside){
						if(lignevide(new Cas(7,7),ro.pos))
							if(!ischeck()){
								ro.pos=new Cas(7,5);
								if(!ischeck()){
									ro.pos=new Cas(7,6);
									if(!ischeck()){
										ro.pos=new Cas(7,4);
										return true;
									}
								}
								ro.pos=new Cas(7,4);
							}
					}
						
				}
			}
		}
		else{
			if(ro.pos.egal(new Cas(0,4))){
				if(casef.egal(new Cas(0,2))){
					if(ro.queenside){
						if(lignevide(new Cas(0,0),ro.pos))
							if(!ischeck()){
								ro.pos=new Cas(0,3);
								if(!ischeck()){
									ro.pos=new Cas(0,2);
									if(!ischeck()){
										ro.pos=new Cas(0,4);
										return true;
									}
								}
								ro.pos=new Cas(0,4);
							}
					}
				}
				else if(casef.egal(new Cas(0,6))){
					if(ro.kingside){
						if(lignevide(new Cas(0,7),ro.pos))
							if(!ischeck()){
								ro.pos=new Cas(0,5);
								if(!ischeck()){
									ro.pos=new Cas(0,6);
									if(!ischeck()){
										ro.pos=new Cas(0,4);
										return true;
									}
								}
								ro.pos=new Cas(0,4);
							}
					}
						
				}
			}
		}
		return false;
	}
	
	public boolean canmove(){
		for(int i=0;i<6;i++){
			for(int j=0;j<pieces.get(turn).get(i).size();j++){
				Piece pc=(Piece)(pieces.get(turn).get(i).get(j));
				Vector<Cas> vect=pc.validmoves();
				for(int k=0;k<vect.size();k++){
					Cas casef=(Cas)(vect.get(k));
					selection=new Cas(pc.pos);
					int valid;
					if((valid=validmove(casef))>0){
						if(valid==1){
							Piece pcf=contenucase(casef);
							if(pcf==null){
								pc.pos=new Cas(casef);
								if(!ischeck()){
									pc.pos=new Cas(selection);
									selection=null;
									return true;
								}
								pc.pos=new Cas(selection);
							}
							else if(pcf.coul!=pc.coul){
								pieces.get(pcf.coul).get(pcf.ind).remove(pcf);
								pc.pos=new Cas(casef);
								if(!ischeck()){
									pieces.get(pcf.coul).get(pcf.ind).add(pcf);
									pc.pos=new Cas(selection);
									selection=null;
									return true;
								}
								pieces.get(pcf.coul).get(pcf.ind).add(pcf);
								pc.pos=new Cas(selection);
								
							}
						}
						else if(valid==2){
							selection=null;
							return true;
						}
						else{
							pieces.get(1-turn).get(Piece.pion).remove(cptpion);
							pc.pos=new Cas(casef);
							if(!ischeck()){
								pieces.get(1-turn).get(Piece.pion).add(cptpion);
								pc.pos=new Cas(selection);
								selection=null;
								return true;
							}
							pieces.get(1-turn).get(Piece.pion).add(cptpion);
							pc.pos=new Cas(selection);
						}
					}
				}
			}
		}
		selection=null;
		return false;
	}
	public int repeated(){
		for(int i=0;i<etats.size();i++){
			ArrayList<ArrayList<ArrayList<Cas>>> pcs=etats.get(i);
			boolean b=true;
			for(int j=0;j<2&&b;j++)
				for(int k=0;k<6&&b;k++)
					for(int l=0;l<pcs.get(j).get(k).size()&&b;l++){
						Cas pc1=pcs.get(j).get(k).get(l);
						boolean different=true;
						for(int m=0;m<pieces.get(j).get(k).size()&&different;m++){
							Piece pc2=pieces.get(j).get(k).get(m);
							if(pc2.pos.egal(pc1))
								different=false;
						}
						if(different)
							b=false;
					}
			if(b){
				return i;
			}
		}
		return -1;
	}
	
	public static Preferences getNode(){
		if(node==null){
			node=Preferences.userRoot().node("ChessPirate");
		}
		return node;
	}
	
	class Clocks extends Thread{
		int clkWhite=0;
		int clkBlack=0;
		boolean isWhite=true;
		boolean stop=false;
		public Clocks(int turn, int m){
			isWhite=(turn==0);
			clkWhite=m*60;
			clkBlack=clkWhite;
			stop=false;
			apercu.horloge1.setText(temptostring(m, 0));
			apercu.horloge2.setText(temptostring(m, 0));
		}
		
		public void run(){
			boucleWhile: while(!isStop()&&clkWhite>0&&clkBlack>0){
				try {
					boolean white=isWhite();
					for(int i=0;i<1000;i+=50){
						if(!isStop()){
							if(white==isWhite())
								sleep(50);
							else{
								continue boucleWhile;
							}
						}
						else
							break boucleWhile;
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if(isWhite()){
					clkWhite--;
					setHorloge(true, temptostring(clkWhite/60, clkWhite%60));
				}
				else{
					clkBlack--;
					setHorloge(false, temptostring(clkBlack/60, clkBlack%60));
				}
			}
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					if(clkWhite==0){
						apercu.message.setText("White's time is over !\n\nBlack wins.");
					}
					else if(clkBlack==0){
						apercu.message.setText("Black's time is over !\n\nWhite wins.");
					}
					apercu.getBoard().setEnabled(false);
				}
			});
		}
		
		private void setHorloge(boolean white, String time){
			final boolean w=white;
			final String t=time;
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					if(w)
						apercu.horloge1.setText(t);
					else
						apercu.horloge2.setText(t);
				}
			});
		}
		
		private synchronized boolean isStop(){
			return stop;
		}
		
		public synchronized void switchClocks(){
			isWhite=!isWhite;
		}
		
		private synchronized boolean isWhite(){
			return isWhite;
		}
		
		public synchronized void stopClocks(){
			stop=true;
		}
		
		private String temptostring(int m, int s){
			String horl=String.valueOf(m)+"  :  ";
			if(s<=9)
				horl+="0";
			horl+=String.valueOf(s);
			return horl;
		}
	}
	
}