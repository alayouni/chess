package newGame;

import icons.Icons;
import interfacegrafic.Chess;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.ComboBoxEditor;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import javax.swing.SwingConstants;

import ro93a.Echiquier;

public class NewGame extends JDialog {

	private static final long serialVersionUID = 1L;
	private JPanel jContentPane = null;
	private JLabel lb = null;
	private JButton ok = null;
	private JComboBox<Integer> choix = null;
	private Chess apercu;

	/**
	 * @param owner
	 */
	public NewGame(Frame owner,String name,boolean b) {
		super(owner,name,b);
		apercu=(Chess)owner;
		this.setLocation(owner.getLocation().x+280,owner.getLocation().y+252);
		initialize();
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setSize(271, 148);
		this.setResizable(false);
		this.setModal(true);
		this.setContentPane(getJContentPane());
		this.setVisible(true);
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			lb = new JLabel();
			lb.setBounds(new Rectangle(6, 19, 133, 34));
			lb.setBackground(Color.black);
			lb.setHorizontalAlignment(SwingConstants.CENTER);
			lb.setFont(new Font("Palatino Linotype", Font.BOLD | Font.ITALIC, 18));
			lb.setForeground(new Color(0,0,204));
			lb.setText("Timer");
			jContentPane = new JPanel();
			jContentPane.setLayout(null);
			jContentPane.setBackground(Color.black);
			jContentPane.add(lb, null);
			jContentPane.add(getOk(), null);
			jContentPane.add(getChoix(), null);
		}
		return jContentPane;
	}

	/**
	 * This method initializes ok	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getOk() {
		if (ok == null) {
			ok = new JButton();
			ok.setBounds(new Rectangle(143, 0, 120, 112));
			ok.setToolTipText("Start");
			ok.setIcon(Icons.start);
			final JDialog cst=this;
			ok.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					Echiquier.getNode().putInt(Echiquier.TIMER_KEY, (Integer)(choix.getSelectedItem()));
					cst.dispose();
					Point loc=apercu.getLocation();
					apercu.dispose();
					apercu.ech=new Echiquier((Integer)(choix.getSelectedItem()));
					apercu.ech.apercu.setLocation(loc);
				}
			});
			
		}
		return ok;
	}

	/**
	 * This method initializes choix	
	 * 	
	 * @return javax.swing.JComboBox	
	 */
	private JComboBox<Integer> getChoix() {
		
			choix = new JComboBox<Integer>();
			choix.setBackground(Color.black);
			for(int i=1;i<31;i++){
				choix.addItem(new Integer(i));
			}
			choix.setRenderer(new ListCellRenderer<Integer>() {
				public Component getListCellRendererComponent(
						JList<? extends Integer> list, Integer value,
						int index, boolean isSelected, boolean cellHasFocus) {
					JButton lab=new JButton(""+(index+1));
					lab.setHorizontalAlignment(SwingConstants.CENTER);
					lab.setFont(new Font("Palatino Linotype", Font.BOLD | Font.ITALIC, 18));
					lab.setForeground(new Color(26, 234, 240));
					lab.setBackground(Color.black);
					return lab;
				}
			});
			choix.setLocation(new Point(6, 60));
			choix.setSize(new Dimension(133, 35));
			
			int timer=0;
			try{
				FileReader fr=new FileReader("C:\\pieces\\Time.txt");
				BufferedReader br=new BufferedReader(fr);
				timer=Integer.parseInt(br.readLine());
				br.close();
			}
			catch(FileNotFoundException e){
				timer=10;
			}
			catch(IOException e){
				timer=10;
			}
			catch(NumberFormatException e){
				timer=10;
			}
		choix.setSelectedIndex(timer-1);
		choix.setEditable(true);
		choix.setFont(new Font("Palatino Linotype", Font.BOLD | Font.ITALIC, 16));
		choix.setEditor(new ComboBoxEditor(){
			JButton lab=new JButton(String.valueOf(choix.getSelectedIndex()+1)+" Minutes");
			public void addActionListener(ActionListener l) {
			}

			public Component getEditorComponent() {
				lab.setHorizontalAlignment(SwingConstants.CENTER);
				lab.setFocusable(false);
				lab.setFont(new Font("Palatino Linotype", Font.BOLD | Font.ITALIC, 16));
				lab.setForeground(new Color(26, 234, 240));
				lab.setBackground(Color.black);
				return lab;
			}

			public Object getItem() {
				return null;
			}

			@Override
			public void removeActionListener(ActionListener l) {
			}

			@Override
			public void selectAll() {
			}

			@Override
			public void setItem(Object anObject) {
				lab.setText(String.valueOf(choix.getSelectedIndex()+1)+" Minutes");
			}
			
		});
		
		return choix;
	}

}  //  @jve:decl-index=0:visual-constraint="10,10"
