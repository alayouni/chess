package interfacegrafic;

import icons.Icons;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

import newGame.NewGame;
import pieceabstraite.Piece;
import ro93a.Echiquier;
import casepack.Cas;

import components.dragndrop.DragDescriptor;
import components.dragndrop.DragEffect;
import components.dragndrop.DragEvent;
import components.dragndrop.DragSource;
import components.dragndrop.DragnDropManager;
import components.dragndrop.DropDescriptor;
import components.dragndrop.DropEvent;
import components.dragndrop.DropTarget;
import components.dragndrop.MyGlassPane;

public class Chess extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel jContentPane = null;
	private Board board = null;
	private Cas selection=new Cas(10,10);
	private Cas kech=new Cas(10,10);
	private Cas[] last={new Cas(10,10),new Cas(10,10)};
	public Echiquier ech;
	public JTextPane horloge1 = null;
	public JTextPane horloge2 = null;
	private JButton newgame = null;
	public Style stl;
	public Style stl1;
	private JLabel cstnoir = null;
	private JLabel cstbanc = null;
	private JTextPane horizontale1 = null;
	private JTextPane vertical2 = null;
	private JTextPane vertical1 = null;
	private JTextPane horizontal2 = null;
	public JTextPane message = null;
	
	
	@SuppressWarnings("serial")
	public class Board extends JPanel implements DragSource, DropTarget{
		private DropDescriptor dropDesc=null;
		private DragDescriptor dragDesc=null;
		
		public Board(){
			super();
			setBounds(30,30,560,560);
		}
		final Color cyan=new Color(0,255,255);
		protected void paintComponent(Graphics g){
			final int caseLength=Icons.bishopBlack.getIconWidth();
			boolean white=true;
			for(int i=0;i<8;i++){
				white=!white;
				for(int j=0;j<8;j++){
					int margin=0;
					if(last[0].egal(j, i)||last[1].egal(j,i)){
						g.setColor(cyan);
						margin=3;
						g.fillRect(i*caseLength, j*caseLength, caseLength, caseLength);
					}
					
					white=!white;
					if(selection.egal(j,i))
						g.setColor(Color.yellow);
					else if(kech.egal(j,i))
						g.setColor(Color.red);
					else
						g.setColor(white?Color.WHITE:Color.BLACK);
					g.fillRect(i*caseLength+margin, j*caseLength+margin, caseLength-2*margin, caseLength-2*margin);
						
				}
			}
			for(ArrayList<ArrayList<Piece>> l1:ech.pieces){
				for(ArrayList<Piece> l2:l1){
					for(Piece p:l2){
						Piece.drawPiece(g, p.pos.c*caseLength, p.pos.l*caseLength, p.ind, p.coul==0?true:false);
					}
				}
			}
		}
		
		public DropDescriptor getDropDescriptor() {
			return dropDesc;
		}
		
		public void setDropDescriptor(DropDescriptor dropDescriptor) {
			dropDesc=dropDescriptor;
		}
		
		public DragDescriptor getDragDescriptor() {
			return dragDesc;
		}
		
		public void setDragDescriptor(DragDescriptor dragDescriptor) {
			dragDesc=dragDescriptor;
		}
	}
	
	
	
	

	public Board getBoard() {
		if (board == null) {
			board=new Board();
		}
		return board;
	}

	public Chess(Echiquier ec) {
		super();
		ech=ec;
		initialize();
	}
	
	final Dimension dim=new Dimension(Icons.bishopBlack.getIconWidth(),Icons.bishopBlack.getIconHeight());
	
	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setContentPane(getJContentPane());
		this.setGlassPane(new MyGlassPane());
		
		getBoard().setDragDescriptor(new DragDescriptor() {
			
			MyDragEffect dragEffect=null;
			Piece piece=null;
			
			public void setDraggedContent(DragEvent e) {
				piece=null;
				if(e!=null){
					final int x=e.getLocation().x/dim.width;
					final int y=e.getLocation().y/dim.height;
					piece=ech.analyser(new Cas(y,x));
				}
			}
			
			
			class MyDragEffect implements DragEffect{
				private DragEvent e=null;
				public void paintEffect(Graphics g) {
					if(e!=null){
						if(piece!=null){
							Piece.drawPiece(g, -dim.width/2, 10-dim.height, piece.ind, piece.coul==0);
						}
					}
				}
				
				public void setDragEvent(DragEvent e){
					this.e=e;
				}
				
			}
			
			public void setDragEffect(DragEvent e) {
				if(dragEffect==null){
					dragEffect=new MyDragEffect();
				}
				dragEffect.setDragEvent(e);
			}
			
			
			public Piece getDraggedContent() {
				return piece;
			}
			
			
			public MyDragEffect getDragEffect() {
				return dragEffect;
			}
			
			
			public Component getComponentSourceOfDrag() {
				return getBoard();
			}
			
			
			public void dropPerformed(DropEvent e) {
				piece=null;
			}


			
			public void dropUnsupported() {
				//un-select
				select(new Cas(10,10));
				ech.selection=null;
			}
		});
		
		DragnDropManager.installDragEffect(getBoard());
		
		getBoard().setDropDescriptor(new DropDescriptor() {
			public void updateDropEffect(DropEvent e) {}
			
			
			public boolean isDropEventSupported(DropEvent e) {
				final int x=e.getLocation().x/dim.width;
				final int y=e.getLocation().y/dim.height;
				if(ech.selection!=null)
					return ech.validmove(new Cas(y,x))!=0;
				return false;
			}
			
			
			public void fireContentDropped(DropEvent e) {
				final int x=e.getLocation().x/dim.width;
				final int y=e.getLocation().y/dim.height;
				ech.analyser(new Cas(y,x));
				//un-select
				select(new Cas(10,10));
				ech.selection=null;
			}
		});
		
		
		this.pack();
		this.setTitle("Chess");
		this.setResizable(false);
		this.setSize(new Dimension(830, 652));
		Toolkit tk=Toolkit.getDefaultToolkit();
		Dimension d=tk.getScreenSize();
		this.setLocation((d.width-728)/2,(d.height-680)/2);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
		this.setIconImage(Icons.logo.getImage());
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			cstbanc = new JLabel();
			cstbanc.setBounds(new Rectangle(745, 20, 70, 70));
			cstbanc.setIcon(Icons.kingWhite);
			cstnoir = new JLabel();
			cstnoir.setBounds(new Rectangle(745, 110, 70, 70));
			cstnoir.setIcon(Icons.kingBlack);
			horloge2 = new JTextPane();
			horloge2.setBackground(Color.black);
			horloge2.setLocation(new Point(635, 140));
			horloge2.setFont(new Font("Dialog", Font.PLAIN, 12));
			horloge2.setSize(new Dimension(100, 70));
			
			
			Style defaut = horloge2.getStyle("default");
			StyledDocument sdoc = (StyledDocument)horloge2.getDocument();
			
			stl=horloge2.addStyle("stl",defaut);
			StyleConstants.setFontFamily(stl,"Palatino Linotype");
		    StyleConstants.setFontSize(stl,24);
		    StyleConstants.setItalic(stl,true);
			StyleConstants.setBold(stl,false);
			StyleConstants.setAlignment(stl,StyleConstants.ALIGN_CENTER);
			sdoc.setParagraphAttributes(0,12,stl,true);
			horloge2.setDisabledTextColor(new Color(194,164,83));
			horloge2.setEnabled(false);
			
			horloge1 = new JTextPane();
			horloge1.setBackground(Color.black);
			horloge1.setLocation(new Point(635, 50));
			horloge1.setSize(new Dimension(100, 70));

			Style defaut1 = horloge1.getStyle("default");
			StyledDocument sdoc1 = (StyledDocument)horloge1.getDocument();
			
			stl1=horloge1.addStyle("stl1",defaut1);
			StyleConstants.setFontFamily(stl1,"Palatino Linotype");
		    StyleConstants.setFontSize(stl1,24);
		    StyleConstants.setItalic(stl1,true);
			StyleConstants.setBold(stl1,false);
			StyleConstants.setAlignment(stl1,StyleConstants.ALIGN_CENTER);
			sdoc1.setParagraphAttributes(0,10,stl1,true);
			horloge1.setDisabledTextColor(new Color(213,210,211));
			horloge1.setEnabled(false);
			
			jContentPane = new JPanel();
			jContentPane.setLayout(null);
			jContentPane.add(getBoard());
			jContentPane.add(horloge1, null);
			jContentPane.add(horloge2, null);
			
			jContentPane.setBackground(Color.black);
			jContentPane.add(getNewgame(), null);
			jContentPane.add(cstnoir, null);
			jContentPane.add(cstbanc, null);
			jContentPane.add(getHorizontale1(), null);
			jContentPane.add(getVertical2(), null);
			jContentPane.add(getVertical1(), null);
			jContentPane.add(getHorizontal2(), null);
			jContentPane.add(getMessage(), null);
		}
		return jContentPane;
	}
	
	public void select(Cas slct){
		selection=new Cas(slct);
		getBoard().repaint();
	}
	
	public void move(Cas casei,Cas casef){
		last[0]=new Cas(casei);
		last[1]=new Cas(casef);
		selection=new Cas(10,10);
		if(!kech.egal(new Cas(10,10))){
			kech=new Cas(10,10);
		}
		updateView();
	}
	
	public void kech(Cas pos){
		kech=new Cas(pos);
		updateView();
	}
	
	public void updateView(){
		getBoard().repaint();
	}

	/**
	 * This method initializes newgame	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getNewgame() {
		if (newgame == null) {
			newgame = new JButton();
			newgame.setLocation(660, 498);
			newgame.setToolTipText("New Game");
			newgame.setMnemonic(KeyEvent.VK_UNDEFINED);
			newgame.setSize(Icons.contour.getIconWidth(),Icons.contour.getIconHeight());
			newgame.setIcon(Icons.contour);
			final JFrame cst=this;
			newgame.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					new NewGame(cst,"New Game",false);
				}
			});
		}
		return newgame;
	}

	/**
	 * This method initializes horizontale1	
	 * 	
	 * @return javax.swing.JTextPane	
	 */
	private JTextPane getHorizontale1() {
		if (horizontale1 == null) {
			horizontale1 = new JTextPane();
			horizontale1.setLocation(new Point(0, 0));
			horizontale1.setBackground(Color.lightGray);
			horizontale1.setFont(new Font("Comic Sans MS", Font.BOLD | Font.ITALIC, 18));
			horizontale1.setText("       A        B       C       D       E        F       G       H");
			horizontale1.setEnabled(false);
			horizontale1.setSize(new Dimension(619, 30));
			horizontale1.setDisabledTextColor(Color.black);
		}
		return horizontale1;
	}

	/**
	 * This method initializes vertical2	
	 * 	
	 * @return javax.swing.JTextPane	
	 */
	private JTextPane getVertical2() {
		if (vertical2 == null) {
			vertical2 = new JTextPane();
			vertical2.setLocation(new Point(589, 0));
			vertical2.setBackground(Color.lightGray);
			vertical2.setFont(new Font("Comic Sans MS", Font.BOLD | Font.ITALIC, 18));
			vertical2.setEnabled(false);
			vertical2.setSize(new Dimension(30, 619));
			vertical2.setDisabledTextColor(Color.black);
			Style defaut = vertical2.getStyle("default");
			StyledDocument sdoc = (StyledDocument)vertical2.getDocument();
		    Style stl=vertical2.addStyle("stl",defaut);
		    StyleConstants.setSpaceAbove(stl,45f);
		    StyleConstants.setFirstLineIndent(stl,5);
		    sdoc.setParagraphAttributes(0,0,stl,true);
		    try {
				sdoc.insertString(0,"1\n2\n3\n4\n5\n6\n7\n8",stl);
			} catch (BadLocationException e) {}
		}
		return vertical2;
	}

	/**
	 * This method initializes vertical1	
	 * 	
	 * @return javax.swing.JTextPane	
	 */
	private JTextPane getVertical1() {
		if (vertical1 == null) {
			vertical1 = new JTextPane();
			vertical1.setLocation(new Point(0, 0));
			vertical1.setBackground(Color.lightGray);
			vertical1.setFont(new Font("Comic Sans MS", Font.BOLD | Font.ITALIC, 18));
			vertical1.setForeground(Color.black);
			vertical1.setEnabled(false);
			vertical1.setSize(new Dimension(30, 619));
			vertical1.setDisabledTextColor(Color.black);
			Style defaut = vertical1.getStyle("default");
			StyledDocument sdoc = (StyledDocument)vertical1.getDocument();
		    Style stl=vertical1.addStyle("stl",defaut);
		    StyleConstants.setSpaceAbove(stl,45f);
		    StyleConstants.setFirstLineIndent(stl,5);
		    sdoc.setParagraphAttributes(0,0,stl,true);
		    try {
				sdoc.insertString(0,"1\n2\n3\n4\n5\n6\n7\n8",stl);
			} catch (BadLocationException e) {}
		}
		return vertical1;
	}

	/**
	 * This method initializes horizontal2	
	 * 	
	 * @return javax.swing.JTextPane	
	 */
	private JTextPane getHorizontal2() {
		if (horizontal2 == null) {
			horizontal2 = new JTextPane();
			horizontal2.setLocation(new Point(0, 589));
			horizontal2.setBackground(Color.lightGray);
			horizontal2.setEnabled(false);
			horizontal2.setFont(new Font("Comic Sans MS", Font.BOLD | Font.ITALIC, 18));
			horizontal2.setForeground(Color.black);
			horizontal2.setText("       A        B       C       D       E        F       G       H");
			horizontal2.setSize(new Dimension(619, 30));
			horizontal2.setDisabledTextColor(Color.black);
		}
		return horizontal2;
	}


	/**
	 * This method initializes message	
	 * 	
	 * @return javax.swing.JTextPane	
	 */
	private JTextPane getMessage() {
		if (message == null) {
			message = new JTextPane();
			message.setBounds(new Rectangle(631, 247, 183, 179));
			message.setEnabled(false);
			message.setBackground(Color.black);
			message.setFont(new Font("Palatino Linotype", Font.BOLD | Font.ITALIC, 26));
			message.setDisabledTextColor(Color.red);
		}
		return message;
	}

}  //  @jve:decl-index=0:visual-constraint="10,10"  
