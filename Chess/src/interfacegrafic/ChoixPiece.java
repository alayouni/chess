package interfacegrafic;

import icons.Icons;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JWindow;

import pieceabstraite.Piece;
import pieces.Cheval;
import pieces.Fou;
import pieces.Reine;
import pieces.Tour;
import ro93a.Echiquier;
import casepack.Cas;


public class ChoixPiece extends JWindow {

	private static final long serialVersionUID = 1L;
	private JPanel jContentPane = null;
	private JButton breine = null;
	private JButton btour = null;
	private JButton bcheval = null;
	private JButton bfou = null;
	private boolean isWhite;
	private Cas pos;
	private Echiquier ech;

	/**
	 * @param owner
	 */
	public ChoixPiece(JFrame owner,Piece pc,Echiquier ec) {
		super(owner);
		if(pc.coul==0)
			isWhite=true;
		else
			isWhite=false;
		pos=new Cas(pc.pos);
		ech=ec;
		initialize();
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setSize(70, 280);
		this.setVisible(true);
		this.setContentPane(getJContentPane());
		this.requestFocus();
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			GridLayout gridLayout = new GridLayout(4,1,0,0);
			jContentPane = new JPanel();
			jContentPane.setLayout(gridLayout);
			jContentPane.add(getBreine(), null);
			jContentPane.add(getBtour(), null);
			jContentPane.add(getBcheval(), null);
			jContentPane.add(getBfou(), null);
		}
		return jContentPane;
	}

	/**
	 * This method initializes breine	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getBreine() {
		if (breine == null) {
			breine = new JButton();
			breine.setBackground(Color.WHITE);
			breine.setIcon(isWhite?Icons.queenWhite:Icons.queenBlack);
			final ChoixPiece cho=this;
			breine.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					Reine rei=new Reine(getcouleur());
					rei.pos=new Cas(pos);
					ech.entrerpiece(rei);
					cho.dispose();
				}
			});
		}
		return breine;
	}

	/**
	 * This method initializes btour	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getBtour() {
		if (btour == null) {
			btour = new JButton();
			btour.setBackground(Color.WHITE);
			btour.setIcon(isWhite?Icons.towerWhite:Icons.towerBlack);
			final ChoixPiece cho=this;
			btour.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					Tour to=new Tour(getcouleur());
					to.pos=new Cas(pos);
					ech.entrerpiece(to);
					cho.dispose();
				}
			});
		}
		return btour;
	}

	/**
	 * This method initializes bcheval	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getBcheval() {
		if (bcheval == null) {
			bcheval = new JButton();
			bcheval.setBackground(Color.WHITE);
			bcheval.setIcon(isWhite?Icons.knightWhite:Icons.knightBlack);
			final ChoixPiece cho=this;
			bcheval.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					Cheval chv=new Cheval(getcouleur());
					chv.pos=new Cas(pos);
					ech.entrerpiece(chv);
					cho.dispose();
				}
			});
		}
		return bcheval;
	}

	/**
	 * This method initializes bfou	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getBfou() {
		if (bfou == null) {
			bfou = new JButton();
			bfou.setBackground(Color.WHITE);
			bfou.setIcon(isWhite?Icons.bishopWhite:Icons.bishopBlack);
			final ChoixPiece cho=this;
			bfou.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					Fou fo=new Fou(getcouleur());
					fo.pos=new Cas(pos);
					ech.entrerpiece(fo);
					cho.dispose();
				}
			});
		}
		return bfou;
	}
	
	private short getcouleur(){
		
		return (short)(isWhite?0:1);
	}

}  //  @jve:decl-index=0:visual-constraint="10,10"
