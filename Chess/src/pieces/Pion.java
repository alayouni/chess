package pieces;

import java.util.Vector;

import casepack.Cas;

import pieceabstraite.Piece;


public class Pion extends Piece{
	public int dernier;
	public Pion(short col){
		coul=col;
		if(col==0){
			dernier=0;
		}
		else{
			dernier=7;
		}
		ind=pion;
	}
	
	public boolean validmove(Cas casef) {
		if(coul==0&&casef.c==pos.c){
			if(pos.l-casef.l==1)
				return true;
			else if(pos.l==6&&casef.l==4)
				return true;
		}
		else if(coul==1&&casef.c==pos.c){
			if(pos.l-casef.l==-1)
				return true;
			else if(pos.l==1&&casef.l==3)
				return true;
			
		}
		return false;
	}
	
	public Vector<Cas> validmoves(){
		Vector<Cas> vect=new Vector<Cas>();
		if(coul==0&&pos.l==6)
			vect.add(new Cas(4,pos.c));
		else if(coul==1&&pos.l==1)
			vect.add(new Cas(3,pos.c));
		vect.add(new Cas(pos.l+2*coul-1,pos.c));
		if(pos.c-1>=0)
			vect.add(new Cas(pos.l+2*coul-1,pos.c-1));
		if(pos.c+1<=7)
			vect.add(new Cas(pos.l+2*coul-1,pos.c+1));
		return vect;
	}


}
