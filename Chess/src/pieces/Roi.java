package pieces;
import java.util.Vector;

import casepack.Cas;

import pieceabstraite.Piece;


public class Roi extends Piece{
	public boolean kingside=true;
	public boolean queenside=true;
	public Roi(short col){
		coul=col;
		ind=roi;
	}
	
	public boolean validmove(Cas casef) {
		if(abs(casef.l-pos.l)<=1&&abs(casef.c-pos.c)<=1&&!pos.egal(casef))
			return true;
		return false;
	}
	
	public Vector<Cas> validmoves(){
		Vector<Cas> vect=new Vector<Cas>();
		for(int i=-1;i<2;i++){
			for(int j=-1;j<2;j++){
				if(j!=0||i!=0){
					Cas move=new Cas(pos.l+i,pos.c+j);
					if(move.l>=0&&move.l<=7)
						if(move.c>=0&&move.c<=7)
							vect.add(move);
				}
			}
		}
		return vect;
	}
}
