package pieces;

import java.util.Vector;

import casepack.Cas;

import pieceabstraite.Piece;


public class Cheval extends Piece{
	public Cheval(short col){
		coul=col;
		ind=cheval;
	}
	
	public boolean validmove(Cas casef) {
		boolean bool=(abs(pos.l-casef.l)==1)&&(abs(pos.c-casef.c)==2);
		bool=bool||(abs(pos.l-casef.l)==2)&&(abs(pos.c-casef.c)==1);
		return bool;
	}
	
	public Vector<Cas> validmoves(){
		Vector<Cas> vect=new Vector<Cas>();
		int[] x={-2,-1,1,2};
		for(int i=0;i<4;i++){
			int y=-abs(x[i])+3;
			for(int j=0;j<2;y*=-1,j++){
				Cas pos2=new Cas(pos.l+x[i],pos.c+y);
				if(pos2.l>=0&&pos2.l<8)
					if(pos2.c>=0&&pos2.c<8)
						vect.add(pos2);
			}
		}
		return vect;
	}
}
