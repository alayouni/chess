package pieces;
import java.util.Vector;

import casepack.Cas;

import pieceabstraite.Piece;


public class Reine extends Piece{
	public Reine(short col){
		coul=col;
		ind=reine;
	}
	
	public boolean validmove(Cas casef) {
		if((memeligne(pos,casef)||memediagonal(pos,casef))&&!pos.egal(casef))
			return true;
		return false;
	}
	
	public Vector<Cas> validmoves(){
		Vector<Cas> vect=new Vector<Cas>();
		int[] ind={-1,1};
		for(int i=0;i<2;i++){
			for(int j=0;j<2;j++){
				for(int x=pos.l+ind[i],y=pos.c+ind[j];x>=0&&x<8&&y>=0&&y<8;x+=ind[i],y+=ind[j])
					vect.add(new Cas(x,y));
			}
		}
		for(int i=0;i<2;i++){
			for(int x=pos.l+ind[i];x>=0&&x<8;x+=ind[i])
				vect.add(new Cas(x,pos.c));
			for(int y=pos.c+ind[i];y>=0&&y<8;y+=ind[i])
				vect.add(new Cas(pos.l,y));
		}
		
		return vect;
	}

}
