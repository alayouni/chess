package pieces;

import java.util.Vector;

import casepack.Cas;

import pieceabstraite.Piece;


public class Fou extends Piece{
	public Fou(short col){
		coul=col;
		ind=fou;
	}
	
	public boolean validmove(Cas casef) {
		if(memediagonal(pos,casef)&&!pos.egal(casef))
			return true;
		return false;
	}
	public Vector<Cas> validmoves(){
		Vector<Cas> vect=new Vector<Cas>();
		int[] ind={-1,1};
		for(int i=0;i<2;i++){
			for(int j=0;j<2;j++){
				for(int x=pos.l+ind[i],y=pos.c+ind[j];x>=0&&x<8&&y>=0&&y<8;x+=ind[i],y+=ind[j])
					vect.add(new Cas(x,y));
			}
		}
		return vect;
	}

}


