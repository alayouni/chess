package pieces;

import java.util.Vector;

import casepack.Cas;

import pieceabstraite.Piece;


public class Tour extends Piece{
	public Tour(short col){
		coul=col;
		ind=tour;
	}
	
	public boolean validmove(Cas casef) {
		if(memeligne(casef,pos)&&!pos.egal(casef))
			return true;
		return false;
	}
	
	public Vector<Cas> validmoves(){
		Vector<Cas> vect=new Vector<Cas>();
		int[] ind={-1,1};
		for(int i=0;i<2;i++){
			for(int x=pos.l+ind[i];x>=0&&x<8;x+=ind[i])
				vect.add(new Cas(x,pos.c));
			for(int y=pos.c+ind[i];y>=0&&y<8;y+=ind[i])
				vect.add(new Cas(pos.l,y));
		}
		
		return vect;
	}
}
